import enum

import pydantic


class Environment(enum.Enum):
    """Possible environments of app."""

    DEBUG = "DEBUG"  # local debug
    DOCKER = "DOCKER"  # local testing with docker
    PRODUCTION = "PRODUCTION"  # production or production-like environment


class Databases(enum.Enum):
    """Possible databases of app."""

    MONGO = "MONGO"  # Mongo database
    POSTGRESQL = "POSTGRESQL"  # Postgresql database
    IN_MEMORY = "IN_MEMORY"  # Database in memory


class App(pydantic.BaseSettings):
    """General application config."""

    TITLE: str = pydantic.Field()
    VERSION: str = pydantic.Field()
    ENVIRONMENT: Environment = pydantic.Field()
    DATABASE: Databases = pydantic.Field()

    class Config:  # noqa
        env_prefix = "BE_APP_"
        frozen = True


class MongoDatabase(pydantic.BaseSettings):
    """Configuration for mongo database."""

    USERNAME: str = pydantic.Field()
    PASSWORD: str = pydantic.Field()
    HOST: str = pydantic.Field()
    PORT: int = pydantic.Field()
    DATABASE: str = pydantic.Field()

    def connection_url(self) -> str:
        """Get database connection string."""
        if self.USERNAME == "":
            return f"mongodb://{self.HOST}:{self.PORT}"

        return f"mongodb://{self.USERNAME}:{self.PASSWORD}@{self.HOST}:{self.PORT}"

    def connection_url_without_user(self) -> str:
        """Get connection string in 'mongodb://{self.HOST}:{self.PORT}' format."""
        return f"mongodb://{self.HOST}:{self.PORT}"

    class Config:  # noqa
        env_prefix = "BE_DB_"
        frozen = True


class SQLDatabase(pydantic.BaseSettings):
    """Configuration for database."""

    DRIVER: str = pydantic.Field()
    USERNAME: str = pydantic.Field()
    PASSWORD: pydantic.SecretStr = pydantic.Field()
    HOST: str = pydantic.Field()
    PORT: int = pydantic.Field()
    DATABASE: str = pydantic.Field()
    DROP_ALL: bool = pydantic.Field()
    CREATE_METADATA: bool = pydantic.Field()

    def connection_url(self) -> str:
        """Get database connection string."""
        return "{driver}://{username}:{password}@{host}:{port}/{database}".format(
            driver=self.DRIVER,
            username=self.USERNAME,
            password=self.PASSWORD.get_secret_value(),
            host=self.HOST,
            port=self.PORT,
            database=self.DATABASE,
        )

    class Config:  # noqa
        env_prefix = "BE_DB_"
        frozen = True


class Auth(pydantic.BaseSettings):
    """Configuration for auth mechanisms."""

    SECRET_KEY: pydantic.SecretStr = pydantic.Field()
    ALGORITHM: str = pydantic.Field()
    JWT_LIFE_TIME_MINS: int = pydantic.Field()

    class Config:  # noqa
        env_prefix = "BE_AUTH_"
        frozen = True
