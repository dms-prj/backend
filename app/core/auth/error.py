class WrongPassword(Exception):  # noqa¡¡
    """Raised when user passed wrong password on login."""


class WrongEmail(Exception):  # noqa
    """Raised when user try use unregistered email."""


class UserIsAlreadyRegistered(Exception):  # noqa
    """Raised when user is already registered."""
