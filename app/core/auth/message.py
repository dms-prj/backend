import pydantic


class RegisterUser(pydantic.BaseModel):
    """Register User  message."""

    name: str
    email: pydantic.EmailStr
    password: pydantic.SecretStr


class LoginUser(pydantic.BaseModel):
    """Login User message."""

    email: pydantic.EmailStr
    password: pydantic.SecretStr
