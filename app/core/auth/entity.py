import pydantic


class Identity(pydantic.BaseModel):
    """Identity for user containing auth info.

    Attributes:
        uid (int | None): global unique identifier, None means non-existing identity
        email (str): user email for login
        pwd (SecretStr): hashed password of the user
    """

    uid: str | None
    email: pydantic.EmailStr
    password: str

    def __repr__(self) -> str:
        return (
            f"<Identity(uid={self.uid}, email={self.email}, password={self.password})>"
        )


class TokenPayload(pydantic.BaseModel):
    """JWT token payload."""

    sub: str
    exp: int
