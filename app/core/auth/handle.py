import datetime

from dependency_injector.wiring import Provide, inject
from jose import jwt
from passlib import context

from app import config
from app.core.auth import entity, error, message, repository
from app.core.checklist import entity as checklist_entity
from app.core.checklist import handle as checklist_handle
from app.core.user import entity as user_entity
from app.core.user import handle as user_handle
from app.di import DI


@inject
async def register(
    message: message.RegisterUser,
    repo: repository.Identity = Provide[DI.identity_repository],
) -> str:
    """Register user in the system.

    Args:
        message (message.RegisterUser)

    Returns:
        str: of user.
    """
    user_tmp = await repo.get_by_email(message.email)
    if user_tmp is not None:
        raise error.UserIsAlreadyRegistered()

    hashed_password = _generate_password_hash(message.password.get_secret_value())
    identity_uid = await repo.insert_one(
        entity.Identity(
            email=message.email,
            password=hashed_password,
        )
    )
    user_uid = await user_handle.create(
        user_entity.User(
            name=message.name,
            identity_uid=identity_uid,
        )
    )

    _ = await checklist_handle.create(
        checklist_entity.Checklist(
            user_id=user_uid,
            fluorography=False,
            clean=False,
            no_arrears=False,
            corpse=False,
            documents=False,
            no_disciplinary=False,
        )
    )

    return _generate_token(user_uid)


@inject
async def login(
    message: message.LoginUser,
    repo: repository.Identity = Provide[DI.identity_repository],
) -> tuple[str, str]:
    """Login user in the system.

    Args:
        message (message.LoginUser)

    Returns:
        str: JWT token
    """
    identity = await repo.get_by_email(message.email)
    if identity is None:
        raise error.WrongEmail()

    if (
        _verify_password(message.password.get_secret_value(), identity.password)
        is False
    ):
        raise error.WrongPassword()
    user_ = await user_handle.get_by_identity_uid(identity.uid)
    return user_.name, _generate_token(user_.uid)


@inject
def _verify_password(
    password: str,
    hashed_password: str,
    pwd_context: context.CryptContext = Provide[DI.pwd_context],
) -> bool:
    return pwd_context.verify(password, hashed_password)


@inject
def _generate_password_hash(
    password: str,
    pwd_context: context.CryptContext = Provide[DI.pwd_context],
) -> str:
    return pwd_context.hash(password)


@inject
def _generate_token(
    user_uid: str,
    auth_config: config.Auth = Provide[DI.auth_config],
) -> str:
    exp = datetime.datetime.utcnow() + datetime.timedelta(
        minutes=auth_config.JWT_LIFE_TIME_MINS
    )
    payload = {
        "sub": user_uid,
        "exp": exp,
    }
    return jwt.encode(
        payload,
        auth_config.SECRET_KEY.display(),
        algorithm=auth_config.ALGORITHM,
    )


@inject
def get_token_payload(
    token: str,
    auth_config: config.Auth = Provide[DI.auth_config],
) -> entity.TokenPayload:
    """Get token payload."""
    return entity.TokenPayload(
        **jwt.decode(
            token,
            auth_config.SECRET_KEY.display(),
            algorithms=[auth_config.ALGORITHM],
        )
    )
