import typing

from app.core.auth import entity


class Identity(typing.Protocol):
    """Repository for Identities."""

    async def insert_one(self, identity: entity.Identity) -> str:
        """Insert new identity into database.

        Args:
            identity (Identity): to insert

        Returns:
            str: id of inserted identity
        """

    async def get_by_uid(self, uid: str) -> entity.Identity | None:
        """Get one identity by uid.

        Args:
            uid (str): of identity.

        Returns:
            Identity | None
        """

    async def get_by_email(self, email: str) -> entity.Identity | None:
        """Get one identity by email.

        Args:
            email (str): of identity.

        Returns:
            Identity | None
        """
