import typing

import pydantic

from app.core.room import entity


class Room(typing.Protocol):
    """Repository for room."""

    async def get_rooms(
        self,
        dormitory: pydantic.PositiveInt | None,
        number: str | None,
        gender: str | None,
        capacity: pydantic.PositiveInt | None,
        one_place: bool | None,
        two_places: bool | None,
        three_places: bool | None,
        page: pydantic.PositiveInt,
        per_page: pydantic.PositiveInt,
    ) -> list[entity.Room]:
        """Get rooms of the dormitory.

        Args:
            dormitory (positive int | None): dormitory name
            number (str | None): number of room
            gender (str | None): gender of room residents
            capacity (int | None): capacity of room
            one_place (bool): does room have one free place
            two_places (bool): does room have two free places
            three_places (bool): does room have three free places
            page (positive int): number of page
            per_page (positive int): size of page

        Returns:
            List of rooms
        """

    async def get_room_count(
        self,
        dormitory: pydantic.PositiveInt | None,
        number: str | None,
        gender: str | None,
        capacity: pydantic.PositiveInt | None,
        one_place: bool | None,
        two_places: bool | None,
        three_places: bool | None,
    ) -> pydantic.NonNegativeInt:
        """Get count of rooms.

        Args:
            dormitory (positive int | None): dormitory name
            number (str | None): number of room
            gender (str | None): gender of room residents
            capacity (int | None): capacity of room
            one_place (bool): does room have one free place
            two_places (bool): does room have two free places
            three_places (bool): does room have three free places

        Returns:
            Count of rooms
        """
