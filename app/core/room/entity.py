import enum

import pydantic


class Gender(str, enum.Enum):
    """Enum class for genders."""

    male = "Мужской"
    female = "Женский"
    undefined = "Общий"


class Room(pydantic.BaseModel):
    """Domain model for room.

    Attributes:
        uid (str): global unique identifier
        number (str): number of room
        dormitory (positive int): name of the dormitory
        gender (Gender): gender of room residents
        freespace (positive int): number of free places in the room
        capacity (positive int): capacity of the room
    """

    uid: str
    number: str
    dormitory: pydantic.PositiveInt
    gender: Gender
    freespace: pydantic.PositiveInt
    capacity: pydantic.PositiveInt
