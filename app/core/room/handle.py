import pydantic
from dependency_injector.wiring import Provide, inject

from app.core.room import entity, repository
from app.di import DI


@inject
async def get_rooms(
    dormitory: pydantic.PositiveInt | None,
    number: str | None,
    gender: str | None,
    capacity: pydantic.PositiveInt | None,
    one_place: bool | None,
    two_places: bool | None,
    three_places: bool | None,
    page: pydantic.PositiveInt,
    per_page: pydantic.PositiveInt,
    repo: repository.Room = Provide[DI.room_repository],
) -> list[entity.Room]:
    """Get rooms of the dormitory.

    Args:
        dormitory (positive int | None): dormitory name
        number (str | None): number of room
        letter (str | None): letter of room
        gender (str | None): gender of room residents
        capacity (int | None): capacity of room
        one_place (bool): does room have one free place
        two_places (bool): does room have two free places
        three_places (bool): does room have three free places
        page (positive int): number of page
        per_page (positive int): size of page

    Returns:
        List of rooms
    """
    return await repo.get_rooms(
        dormitory,
        number,
        gender,
        capacity,
        one_place,
        two_places,
        three_places,
        page,
        per_page,
    )


@inject
async def get_room_count(
    dormitory: pydantic.PositiveInt | None,
    number: str | None,
    gender: str | None,
    capacity: pydantic.PositiveInt | None,
    one_place: bool | None,
    two_places: bool | None,
    three_places: bool | None,
    repo: repository.Room = Provide[DI.room_repository],
) -> pydantic.NonNegativeInt:
    """Get count of rooms.

    Args:
        dormitory (positive int | None): dormitory name
        number (str | None): number of room
        gender (str | None): gender of room residents
        capacity (int | None): capacity of room
        one_place (bool): does room have one free place
        two_places (bool): does room have two free places
        three_places (bool): does room have three free places

    Returns:
        Count of rooms
    """

    return await repo.get_room_count(
        dormitory,
        number,
        gender,
        capacity,
        one_place,
        two_places,
        three_places,
    )
