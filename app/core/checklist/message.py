import pydantic

from app.core.user import entity as user_entity


class ChangeState(pydantic.BaseModel):
    """Change state message."""

    user_id: user_entity.UserUID
    name: str
    state: bool
