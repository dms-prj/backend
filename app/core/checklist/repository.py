import typing

from app.core.checklist import entity
from app.core.user import entity as user_entity


class Checklist(typing.Protocol):
    """Repository for checklist."""

    async def get_by_user_id(self, user_id: user_entity.UserUID) -> entity.Checklist:
        """Get one checklist by uid."""

    async def insert_one(self, checklist: entity.Checklist) -> entity.ChecklistUID:
        """Insert new checklist."""

    async def change_state(self, user_id: user_entity.UserUID, name: str, state: bool):
        """Change state."""
