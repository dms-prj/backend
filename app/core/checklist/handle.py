from dependency_injector.wiring import Provide, inject

from app.core.checklist import entity, message, repository
from app.core.user import entity as user_entity
from app.di import DI


@inject
async def get_by_user_id(
    user_id: user_entity.UserUID,
    repo: repository.Checklist = Provide[DI.checklist_repository],
) -> entity.Checklist:
    """Get checklist for user."""
    return await repo.get_by_user_id(user_id)


@inject
async def create(
    checklist: entity.Checklist,
    repo: repository.Checklist = Provide[DI.checklist_repository],
) -> entity.Checklist:
    """Create checklist for new user."""
    return await repo.insert_one(checklist)


@inject
async def change_state(
    message: message.ChangeState,
    repo: repository.Checklist = Provide[DI.checklist_repository],
) -> entity.Checklist:
    """Change state for user."""
    return await repo.change_state(message.user_id, message.name, message.state)
