import pydantic

from app.core.user import entity as user_entity

ChecklistUID = str


class Checklist(pydantic.BaseModel):
    """Checklist model."""

    uid: str | None

    user_id: user_entity.UserUID

    fluorography: bool
    clean: bool
    no_arrears: bool
    corpse: bool
    documents: bool
    no_disciplinary: bool
