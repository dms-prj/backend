import pydantic


class NewFeedback(pydantic.BaseModel):
    """New feedback message."""

    message: str
