from dependency_injector.wiring import Provide, inject

from app.core.feedback import entity, message, repository
from app.di import DI


@inject
async def new_feedback(
    feedback: message.NewFeedback,
    repo: repository.Feedback = Provide[DI.feedback_repository],
) -> entity.FeedbackUID:
    """Add new feedback.

    Args:
        message (NewFeedback)

    Returns:
        str: of user.

    """
    feedback_id = await repo.insert_one(entity.Feedback(message=feedback.message))

    return feedback_id
