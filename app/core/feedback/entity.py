import pydantic

FeedbackUID = str


class Feedback(pydantic.BaseModel):
    """Feedback model."""

    uid: FeedbackUID | None
    message: str
