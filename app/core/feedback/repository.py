import typing

from app.core.feedback import entity


class Feedback(typing.Protocol):
    """Repository for feedback."""

    async def insert_one(self, feedback: entity.Feedback) -> entity.FeedbackUID:
        """Insert one feedback.

        Args:
            feedback (Feedback): to insert

        Returns:
            feedback_id: uid of feedback
        """

    async def get_by_uid(self, uid: entity.FeedbackUID) -> entity.Feedback | None:
        """Get one Feedback by uid.

        Args:
            uid (str): uid of feedback

        Returns:
            Feedback | None
        """
