from dependency_injector.wiring import Provide, inject

from app.core.user import entity, repository
from app.di import DI


@inject
async def get_by_uid(
    uid: str,
    repo: repository.User = Provide[DI.user_repository],
) -> entity.User:
    """Get user by id.

    Args:
        uid (str): of user

    Returns:
        User
    """
    return await repo.get_by_uid(uid)


@inject
async def get_by_identity_uid(
    uid: str,
    repo: repository.User = Provide[DI.user_repository],
) -> entity.User:
    """Ger user by identity uid.

    Args:
        uid (str): of identity

    Returns:
        User
    """
    return await repo.get_by_identity_uid(uid)


@inject
async def create(
    user: entity.User,
    repo: repository.User = Provide[DI.user_repository],
) -> str:
    """Create new user.

    Args:
        user (User): to create

    Returns:
        str: uid of user.
    """
    return await repo.insert_one(user)
