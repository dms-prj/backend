import pydantic

UserUID = str


class User(pydantic.BaseModel):
    """Domain model for user.

    Attributes:
        uid (int | None): global unique identifier, None if user does not exist
        name (str): of the user
        identity_uid (int): for accessing user auth info
    """

    uid: str | None
    name: str
    identity_uid: str

    def __repr__(self) -> str:
        return f"<User(uid={self.uid}, name={self.name})>"
