import typing

from app.core.user import entity


class User(typing.Protocol):
    """Repository for user."""

    async def insert_one(self, user: entity.User) -> str:
        """Insert one user.

        Args:
            user (User): to insert

        Returns:
            str: uid of user
        """

    async def get_by_uid(self, uid: str) -> entity.User | None:
        """Get one user by uid.

        Args:
            uid (str): uid of user

        Returns:
            User | None
        """

    async def get_by_identity_uid(self, uid: str) -> entity.User | None:
        """Get one user by identity uid.

        Args:
            uid (str): of identity

        Returns:
            User | None
        """
