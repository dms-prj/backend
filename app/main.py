import fastapi
from dependency_injector import providers

from app import api, config, core, di, service


def create_app() -> fastapi.FastAPI:
    """Create FastAPI instance to run.

    Returns:
        FastAPI: app instance
    """
    container = di.DI()
    app_config = container.app_config()
    app = api.bootstrap(app_config)
    _bootstrap_database(app, container, app_config)

    container.wire(packages=[core, service])

    return app


def _bootstrap_database(
    app: fastapi.FastAPI,
    container: di.DI,
    app_config: config.App,
):
    match app_config.DATABASE:
        case config.Databases.MONGO:
            container.identity_repository.override(
                providers.Singleton(
                    service.repository.mongo.Identity,
                    container.mongo_database,
                )
            )

            container.user_repository.override(
                providers.Singleton(
                    service.repository.mongo.User,
                    container.mongo_database,
                )
            )

            container.feedback_repository.override(
                providers.Singleton(
                    service.repository.mongo.Feedback,
                    container.mongo_database,
                )
            )

            container.room_repository.override(
                providers.Singleton(
                    service.repository.mongo.Room, container.mongo_database
                )
            )

            container.checklist_repository.override(
                providers.Singleton(
                    service.repository.mongo.Checklist, container.mongo_database
                )
            )

        case config.Databases.POSTGRESQL:
            container.identity_repository.override(
                providers.Singleton(
                    service.repository.sql.Identity,
                    container.sql_database.provided.session,
                )
            )
            container.user_repository.override(
                providers.Singleton(
                    service.repository.sql.User,
                    container.sql_database.provided.session,
                )
            )

            app.add_event_handler(
                "startup",
                container.sql_database().create_database,
            )

        case config.Databases.IN_MEMORY:
            container.identity_repository.override(
                providers.Singleton(service.repository.in_memory.Identity)
            )
            container.user_repository.override(
                providers.Singleton(service.repository.in_memory.User)
            )

        case db:
            raise Exception(f"Database {db} not found")
