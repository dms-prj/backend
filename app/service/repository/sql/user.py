import contextlib
import typing

import sqlmodel
from sqlalchemy.ext import asyncio as async_sqlalchemy
from sqlalchemy.future import select

from app.core.user import entity, repository


class _User(sqlmodel.SQLModel, table=True):  # noqa
    __tablename__ = "user"
    uid: int | None = sqlmodel.Field(default=None, primary_key=True, unique=True)
    name: str
    identity_uid: int = sqlmodel.Field(
        unique=True, index=True, foreign_key="identity.uid"
    )

    @staticmethod
    def from_entity(entity: entity.User) -> "_User":  # noqa
        return _User(**entity.dict())

    def to_entity(self: "_User") -> entity.User:  # noqa
        return entity.User(**self.dict())


class User(repository.User):
    """SQL repository for identity."""

    def __init__(
        self,
        session_maker: typing.Callable[
            ..., contextlib.AbstractAsyncContextManager[async_sqlalchemy.AsyncSession]
        ],
    ) -> None:
        self._session_maker = session_maker

    async def get_by_identity_uid(self, uid: int) -> entity.User | None:  # noqa
        async with self._session_maker() as session:  # noqa
            result = await session.execute(
                select(_User).where(_User.identity_uid == uid)
            )
            row = result.first()

            if row is None:
                return None

            _user: _User = row[0]
            return _user.to_entity()

    async def get_by_uid(self, uid: int) -> entity.User | None:  # noqa
        async with self._session_maker() as session:  # noqa
            result = await session.execute(select(_User).where(_User.uid == uid))
            row = result.first()

            if row is None:
                return None

            _user: _User = row[0]
            return _user.to_entity()

    async def insert_one(self, user: entity.User) -> int:  # noqa
        async with self._session_maker() as session:
            session: async_sqlalchemy.AsyncSession
            row = _User.from_entity(user)
            session.add(row)
            await session.commit()
            await session.refresh(row)
            return row.uid
