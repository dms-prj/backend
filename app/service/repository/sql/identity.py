import contextlib
import typing

import sqlmodel
from sqlalchemy.ext import asyncio as async_sqlalchemy
from sqlalchemy.future import select

from app.core.auth import entity, repository


class _Identity(sqlmodel.SQLModel, table=True):  # noqa
    __tablename__ = "identity"
    uid: int | None = sqlmodel.Field(default=None, primary_key=True, unique=True)
    email: str = sqlmodel.Field(unique=True, index=True)
    password: str

    @staticmethod
    def from_entity(entity: entity.Identity) -> "_Identity":  # noqa
        return _Identity(**entity.dict())

    def to_entity(self: "_Identity") -> entity.Identity:  # noqa
        return entity.Identity(**self.dict())


class Identity(repository.Identity):
    """SQL repository for identity."""

    def __init__(
        self,
        session_maker: typing.Callable[
            ..., contextlib.AbstractAsyncContextManager[async_sqlalchemy.AsyncSession]
        ],
    ) -> None:
        self._session_maker = session_maker

    async def insert_one(self, identity: entity.Identity) -> int:  # noqa
        async with self._session_maker() as session:
            session: async_sqlalchemy.AsyncSession
            row = _Identity.from_entity(identity)
            session.add(row)
            await session.commit()
            await session.refresh(row)
            return row.uid

    async def get_by_email(self, email: str) -> entity.Identity | None:  # noqa
        async with self._session_maker() as session:  # noqa
            result = await session.execute(
                select(_Identity).where(_Identity.email == email)
            )
            row = result.first()

            if row is None:
                return None

            _identity: _Identity = row[0]
            return _identity.to_entity()

    async def get_by_uid(self, uid: int) -> entity.Identity | None:  # noqa
        async with self._session_maker() as session:  # noqa
            result = await session.execute(
                select(_Identity).where(_Identity.uid == uid)
            )
            row = result.first()

            if row is None:
                return None

            _identity: _Identity = row[0]
            return _identity.to_entity()
