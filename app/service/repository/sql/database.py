import asyncio
import contextlib
import typing

import sqlmodel
from sqlalchemy import orm
from sqlalchemy.ext import asyncio as async_sqlalchemy

from app import config


class Database:
    """Wrapper around SQLAlchemy for repositories."""

    def __init__(self, sql_database_config: config.SQLDatabase) -> None:
        self._config = sql_database_config
        self._engine = async_sqlalchemy.create_async_engine(
            self._config.connection_url(),
            echo=True,
            future=True,
        )
        self._session_factory = async_sqlalchemy.async_scoped_session(
            orm.sessionmaker(
                autocommit=False,
                autoflush=False,
                bind=self._engine,
                class_=async_sqlalchemy.AsyncSession,
            ),
            scopefunc=asyncio.current_task,
        )

    async def create_database(self) -> None:
        """Create metadata for database."""
        async with self._engine.begin() as connection:
            if self._config.DROP_ALL:
                await connection.run_sync(sqlmodel.SQLModel.metadata.drop_all)

            if self._config.CREATE_METADATA:
                await connection.run_sync(sqlmodel.SQLModel.metadata.create_all)

    @contextlib.asynccontextmanager
    async def session(
        self,
    ) -> typing.Callable[
        ..., contextlib.AbstractAsyncContextManager[async_sqlalchemy.AsyncSession]
    ]:
        """Create managed AsyncSession."""
        async with self._session_factory() as session:
            session: async_sqlalchemy.AsyncSession
            try:
                yield session
            except Exception:
                print("rollback")
                await session.rollback()
                raise
