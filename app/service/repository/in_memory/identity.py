import itertools

from app.core.auth import entity, repository


class Identity(repository.Identity):
    """In memory identity repository."""

    def __init__(self) -> None:
        self._data: dict[int, entity.Identity] = {}
        self._uids = itertools.count()

    async def insert_one(self, identity: entity.Identity) -> int:  # noqa
        uid = next(self._uids)
        identity.uid = uid
        self._data[uid] = identity
        return uid

    async def get_by_email(self, email: str) -> entity.Identity | None:  # noqa
        return next(filter(lambda i: i.email == email, self._data.values()), None)

    async def get_by_uid(self, uid: int) -> entity.Identity:  # noqa
        return self._data.get(uid, None)
