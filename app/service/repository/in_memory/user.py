import itertools

from app.core.user import entity, repository


class User(repository.User):
    """In memory user repository."""

    def __init__(self) -> None:
        self._data: dict[int, entity.User] = {}
        self._uids = itertools.count()

    async def insert_one(self, user: entity.User) -> int:  # noqa
        uid = next(self._uids)
        user.uid = uid
        self._data[uid] = user
        return uid

    async def get_by_uid(self, uid: int) -> entity.User | None:  # noqa
        return self._data.get(uid, None)

    async def get_by_identity_uid(self, uid: int) -> entity.User | None:  # noqa
        return next(filter(lambda u: u.identity_uid == uid, self._data.values()), None)
