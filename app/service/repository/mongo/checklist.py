from app.core.checklist import entity, repository
from app.core.user import entity as user_entity

from .database import Collections, Database


class Checklist(repository.Checklist):
    """Mongo db repository for checklist."""

    def __init__(self, database: Database) -> None:
        self._collection = database.get_collection(Collections.Checklist.value)

    async def insert_one(  # noqa
        self, checklist: entity.Checklist
    ) -> entity.ChecklistUID:
        ans = (
            await self._collection.insert_one(checklist.dict(exclude={"uid"}))
        ).inserted_id
        return entity.ChecklistUID(ans)

    async def get_by_user_id(  # noqa
        self, user_id: user_entity.UserUID
    ) -> entity.Checklist:
        document: dict = await self._collection.find_one({"user_id": user_id})
        if document is None:
            return None

        document["uid"] = str(document.pop("_id"))
        return entity.Checklist(**document)

    async def change_state(  # noqa
        self, user_id: user_entity.UserUID, name: str, state: bool
    ):
        _ = await self._collection.update_one(
            {"user_id": user_id}, {"$set": {name: state}}
        )
        return
