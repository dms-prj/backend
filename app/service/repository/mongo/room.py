import string

import pydantic

from app.core.room import entity, repository
from app.service.repository import mongo


class Room(repository.Room):
    """Mongo repository for room."""

    def __init__(self, database: mongo.Database) -> None:
        self._collection = database.get_collection(mongo.Collections.Room.value)

    async def get_rooms(  # noqa
        self,
        dormitory: pydantic.PositiveInt | None,
        number: str | None,
        gender: str | None,
        capacity: pydantic.PositiveInt | None,
        one_place: bool | None,
        two_places: bool | None,
        three_places: bool | None,
        page: pydantic.PositiveInt,
        per_page: pydantic.PositiveInt,
    ) -> list[entity.Room]:

        free_space: list[bool] = [one_place, two_places, three_places]  # noqa

        letter = None

        if number is not None:
            if number[-1] not in string.digits:
                letter = number[-1]
                number = int(number[:-1])
            else:
                number = int(number)

        if gender == "male":
            gender = entity.Gender.male
        elif gender == "female":
            gender = entity.Gender.female
        elif gender == "any-gender":
            gender = entity.Gender.undefined
        _filter = {
            "$and": [
                {
                    "$or": [
                        {"freespace": _free_space}
                        for _free_space in [
                            (i + 1) if free_space[i] is True else 0 for i in range(3)
                        ]
                    ]
                },
            ],
        }
        if dormitory is not None:
            _filter["dormitory"] = dormitory
        if number is not None:
            _filter["number"] = number
        if capacity is not None:
            _filter["capacity"] = capacity
        if gender is not None:
            _filter["$and"].append(
                {
                    "$or": [
                        {"gender": _gender}
                        for _gender in [entity.Gender.undefined, gender]
                    ]
                }
            )
        if letter is not None:
            _filter["letter"] = letter

        documents: list[dict] = (
            await self._collection.find(_filter)
            .sort([["_id", 1], ["dormitory", 1], ["number", 1], ["letter", 1]])
            .skip(per_page * (page - 1))
            .limit(per_page)
            .to_list(None)
        )

        if documents is None:
            return None

        result = []

        for document in documents:
            document["uid"] = str(document.pop("_id"))
            letter = document.pop("letter")
            document["number"] = str(document["number"])
            if letter is not None:
                document["number"] += letter
            result.append(entity.Room(**document))

        return result

    async def get_room_count(  # noqa
        self,
        dormitory: pydantic.PositiveInt | None,
        number: str | None,
        gender: str | None,
        capacity: pydantic.PositiveInt | None,
        one_place: bool | None,
        two_places: bool | None,
        three_places: bool | None,
    ) -> pydantic.NonNegativeInt:

        free_space: list[bool] = [one_place, two_places, three_places]  # noqa

        letter = None

        if number is not None:
            if number[-1] not in string.digits:
                letter = number[-1]
                number = int(number[:-1])
            else:
                number = int(number)

        if gender == "male":
            gender = entity.Gender.male
        elif gender == "female":
            gender = entity.Gender.female
        elif gender == "any-gender":
            gender = entity.Gender.undefined
        _filter = {
            "$and": [
                {
                    "$or": [
                        {"freespace": _free_space}
                        for _free_space in [
                            (i + 1) if free_space[i] is True else 0 for i in range(3)
                        ]
                    ]
                },
            ],
        }
        if dormitory is not None:
            _filter["dormitory"] = dormitory
        if number is not None:
            _filter["number"] = number
        if capacity is not None:
            _filter["capacity"] = capacity
        if gender is not None:
            _filter["$and"].append(
                {
                    "$or": [
                        {"gender": _gender}
                        for _gender in [entity.Gender.undefined, gender]
                    ]
                }
            )
        if letter is not None:
            _filter["letter"] = letter
        total: pydantic.NonNegativeInt = await self._collection.count_documents(_filter)

        return total
