import bson

from app.core.feedback import entity, repository
from app.service.repository import mongo


class Feedback(repository.Feedback):
    """Mongo db repository for feedback."""

    def __init__(self, database: mongo.Database) -> None:
        self._collection = database.get_collection(mongo.Collections.Feedback.value)

    async def insert_one(self, feedback: entity.Feedback) -> entity.FeedbackUID:  # noqa
        return entity.FeedbackUID(
            (
                await self._collection.insert_one(feedback.dict(exclude={"uid"}))
            ).inserted_id
        )

    async def get_by_uid(  # noqa
        self, uid: entity.FeedbackUID
    ) -> entity.Feedback | None:
        document: dict = await self._collection.find_one({"_id": bson.ObjectId(uid)})
        if document is None:
            return None

        document["uid"] = str(document.pop("_id"))
        return entity.Feedback(**document)
