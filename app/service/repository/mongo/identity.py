import bson

from app.core.auth import entity, repository
from app.service.repository import mongo


class Identity(repository.Identity):
    """Mongo db repository for identity."""

    def __init__(self, database: mongo.Database) -> None:
        self._collection = database.get_collection(mongo.Collections.Identity.value)

    async def insert_one(self, identity: entity.Identity) -> str:  # noqa
        # TODO Need to add a check that the email is already in the database
        # Or need add index to mongodb with unique
        return str(
            (
                await self._collection.insert_one(identity.dict(exclude={"uid"}))
            ).inserted_id
        )

    async def get_by_email(self, email: str) -> entity.Identity | None:  # noqa
        document: dict = await self._collection.find_one({"email": email})
        if document is None:
            return None

        document["uid"] = str(document.pop("_id"))
        return entity.Identity(**document)

    async def get_by_uid(self, uid: str) -> entity.Identity | None:  # noqa
        document: dict = await self._collection.find_one({"_id": bson.ObjectId(uid)})
        if document is None:
            return None

        document["uid"] = str(document.pop("_id"))
        return entity.Identity(**document)
