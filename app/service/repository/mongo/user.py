import bson

from app.core.user import entity, repository
from app.service.repository import mongo


class User(repository.User):
    """Mongo db repository for user."""

    def __init__(self, database: mongo.Database) -> None:
        self._collection = database.get_collection(mongo.Collections.User.value)

    async def get_by_identity_uid(self, uid: str) -> entity.User | None:  # noqa
        document: dict = await self._collection.find_one({"identity_uid": uid})

        if document is None:
            return None

        document["uid"] = str(document.pop("_id"))
        return entity.User(**document)

    async def get_by_uid(self, uid: str) -> entity.User | None:  # noqa
        document: dict = await self._collection.find_one({"_id": bson.ObjectId(uid)})

        if document is None:
            return None

        document["uid"] = str(document.pop("_id"))
        return entity.User(**document)

    async def insert_one(self, user: entity.User) -> str:  # noqa
        # TODO Need to add a check that the user is already in the database
        # Or need add index to mongodb with unique
        user_dict = user.dict(exclude={"uid"})

        return str((await self._collection.insert_one(user_dict)).inserted_id)
