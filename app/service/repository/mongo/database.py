import enum

from motor import motor_asyncio

from app import config


class Collections(enum.Enum):
    """Collection names."""

    Identity = "identity"
    User = "user"
    Counters = "counters"
    Feedback = "feedback"
    Room = "room"
    Checklist = "checklist"


class Database:
    """Wrapper around motor(Mongo db) for repositories."""

    def __init__(self, mongo_config: config.MongoDatabase):
        self._config = mongo_config
        self._client = motor_asyncio.AsyncIOMotorClient(self._config.connection_url())
        self._database = self._client[self._config.DATABASE]
        self._counters = self._database[Collections.Counters.value]

    def get_collection(self, collection: Collections):
        """Get collection."""
        return self._database[collection]

    async def get_next_id(self, collection: str):
        """Get generator what return last id in collection."""
        while True:
            yield await self._counters.find_one_and_update(
                filter={"_id": collection}, update={"$inc": {"last_id": 1}}
            )
