from dependency_injector import containers, providers
from passlib import context

from app import config
from app.core.auth.repository import Identity as IdentityRepository
from app.core.checklist.repository import Checklist as ChecklistRepository
from app.core.feedback.repository import Feedback as FeedbackRepository
from app.core.room.repository import Room as RoomRepository
from app.core.user.repository import User as UserRepository
from app.service.repository import mongo, sql


class DI(containers.DeclarativeContainer):
    """Declarative container for dependencies."""

    app_config = providers.Singleton(config.App)
    auth_config = providers.Singleton(config.Auth)
    sql_database_config = providers.Singleton(config.SQLDatabase)
    mongo_database_config = providers.Singleton(config.MongoDatabase)

    pwd_context = providers.Singleton(
        context.CryptContext,
        schemes=["bcrypt"],
        deprecated="auto",
    )

    sql_database = providers.Singleton(
        sql.Database,
        sql_database_config=sql_database_config,
    )

    mongo_database = providers.Singleton(
        mongo.Database,
        mongo_config=mongo_database_config,
    )

    identity_repository = providers.AbstractSingleton(IdentityRepository)
    user_repository = providers.AbstractSingleton(UserRepository)
    feedback_repository = providers.AbstractSingleton(FeedbackRepository)
    room_repository = providers.AbstractSingleton(RoomRepository)
    checklist_repository = providers.AbstractSingleton(ChecklistRepository)
