import fastapi
import jose
import pydantic

from app.core import auth


def bootstrap(app: fastapi.FastAPI) -> fastapi.FastAPI:
    """Initializing error handlers for FastAPI."""
    app.add_exception_handler(jose.JWTError, jwt_error_handler)
    app.add_exception_handler(auth.error.WrongPassword, wrong_password_error_handler)
    app.add_exception_handler(auth.error.WrongEmail, wrong_email_error_handler)
    app.add_exception_handler(
        auth.error.UserIsAlreadyRegistered, user_is_already_registered_error_handler
    )

    return app


class _ErrorResponse(pydantic.BaseModel):
    message: str


async def jwt_error_handler(_: fastapi.Request, error: jose.JWTError):  # noqa
    return fastapi.responses.JSONResponse(
        status_code=fastapi.status.HTTP_401_UNAUTHORIZED,
        content=_ErrorResponse(message=f"JWT error occurred: {error}").dict(),
    )


async def wrong_password_error_handler(  # noqa
    _: fastapi.Request,
    __: auth.error.WrongPassword,
):
    return fastapi.responses.JSONResponse(
        status_code=fastapi.status.HTTP_401_UNAUTHORIZED,
        content=_ErrorResponse(message="Wrong password").dict(),
    )


async def wrong_email_error_handler(  # noqa
    _: fastapi.Request,
    __: auth.error.WrongEmail,
):
    return fastapi.responses.JSONResponse(
        status_code=fastapi.status.HTTP_401_UNAUTHORIZED,
        content=_ErrorResponse(message="User not found").dict(),
    )


async def user_is_already_registered_error_handler(
    _: fastapi.Request,
    __: auth.error.UserIsAlreadyRegistered,
):
    return fastapi.responses.JSONResponse(
        status_code=fastapi.status.HTTP_409_CONFLICT,
        content=_ErrorResponse(message="User is already registered").dict(),
    )
