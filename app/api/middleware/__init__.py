import fastapi
from fastapi.middleware.cors import CORSMiddleware


def bootstrap(app: fastapi.FastAPI) -> fastapi.FastAPI:
    """Initializing middleware."""
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return app
