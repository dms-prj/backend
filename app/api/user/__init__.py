import fastapi

from app.api.user.endpoints import router


def bootstrap(app: fastapi.FastAPI) -> fastapi.FastAPI:
    """Initialize user API."""
    app.include_router(router)
    return app
