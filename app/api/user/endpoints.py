import fastapi

from app.api import deps
from app.core.user import handle

router = fastapi.APIRouter(prefix="/user", tags=["User"])


@router.get("/me")
async def get_me(user_uid: str = fastapi.Depends(deps.current_user_uid)):  # noqa
    return await handle.get_by_uid(user_uid)
