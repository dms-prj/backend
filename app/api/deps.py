import fastapi
from fastapi import security

from app.core.auth import handle

oauth2_scheme = security.OAuth2PasswordBearer(tokenUrl="auth/login")


async def current_user_uid(
    token: str = fastapi.Depends(oauth2_scheme),  # noqa
) -> str:
    """Get current user which verifies auth."""
    return handle.get_token_payload(token).sub
