import pydantic


class RegisterUserRequest(pydantic.BaseModel):  # noqa
    name: str
    email: pydantic.EmailStr
    password: pydantic.SecretStr


class TokenResponse(pydantic.BaseModel):  # noqa
    access_token: str
    token_type: str
    name: str
