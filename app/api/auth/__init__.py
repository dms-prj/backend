import fastapi

from app.api.auth.endpoints import router


def bootstrap(app: fastapi.FastAPI) -> fastapi.FastAPI:
    """Initialize auth API."""
    app.include_router(router)
    return app
