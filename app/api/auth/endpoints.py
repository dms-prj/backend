import fastapi
from fastapi import security

from app.api.auth import contract
from app.core.auth import handle, message

router = fastapi.APIRouter(prefix="/auth", tags=["Auth"])


@router.post("/register")
async def register(body: contract.RegisterUserRequest):  # noqa
    msg = message.RegisterUser(**body.dict())
    return await handle.register(msg)


@router.post("/login")
async def login(form: security.OAuth2PasswordRequestForm = fastapi.Depends()):  # noqa
    msg = message.LoginUser(email=form.username, password=form.password)
    name, token = await handle.login(msg)
    return contract.TokenResponse(  # noqa
        access_token=token, token_type="bearer", name=name
    )
