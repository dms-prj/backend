import fastapi
import toolz

from app import config
from app.api import auth, checklist, error_handler, feedback, middleware, room, user


def bootstrap(app_config: config.App) -> fastapi.FastAPI:
    """Initialize FastAPI application."""
    return toolz.pipe(
        fastapi.FastAPI(
            title=app_config.TITLE,
            version=app_config.VERSION,
        ),
        middleware.bootstrap,
        error_handler.bootstrap,
        auth.bootstrap,
        user.bootstrap,
        feedback.bootstrap,
        room.bootstrap,
        checklist.bootstrap,
    )
