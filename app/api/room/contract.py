import pydantic

from app.core.room import entity


class PageResponse(pydantic.BaseModel):  # noqa

    rooms: list[entity.Room]
    total: pydantic.NonNegativeInt
    page: pydantic.PositiveInt
    perPage: pydantic.PositiveInt  # noqa
