import fastapi

from app.api.room.endpoints import router


def bootstrap(app: fastapi.FastAPI) -> fastapi.FastAPI:
    """Initialize room API."""
    app.include_router(router)
    return app
