import fastapi
import pydantic

from app.api.room import contract
from app.core.room import handle

router = fastapi.APIRouter(tags=["Campus"], prefix="/campus")


@router.get("")
async def get_page(  # noqa
    dormitory: pydantic.PositiveInt
    | None = fastapi.Query(default=None, description="Number of dormitory"),  # noqa
    number: str
    | None = fastapi.Query(  # noqa
        default=None,
        description="Number of room",
    ),
    gender: str
    | None = fastapi.Query(  # noqa
        default=None,
        description="Gender of room residents",
    ),
    capacity: pydantic.PositiveInt
    | None = fastapi.Query(  # noqa
        default=None,
        description="Capacity of room",
    ),
    onePlace: bool = fastapi.Query(  # noqa
        default=True,
        description="Does room have one free place.",
    ),
    twoPlaces: bool = fastapi.Query(  # noqa
        default=True,
        description="Does room have two free places.",
    ),
    threePlaces: bool = fastapi.Query(  # noqa
        default=True,
        description="Does room have three free places.",
    ),
    page: pydantic.PositiveInt = fastapi.Query(  # noqa
        default=1,
        description="Number of page",
    ),
    perPage: pydantic.PositiveInt = fastapi.Query(  # noqa
        default=20,
        description="Size of page",
    ),
):

    total = await handle.get_room_count(
        dormitory,
        number,
        gender,
        capacity,
        onePlace,
        twoPlaces,
        threePlaces,
    )
    rooms = []
    if total > 0:
        rooms = await handle.get_rooms(
            dormitory,
            number,
            gender,
            capacity,
            onePlace,
            twoPlaces,
            threePlaces,
            page,
            perPage,
        )
    return contract.PageResponse(rooms=rooms, total=total, page=page, perPage=perPage)
