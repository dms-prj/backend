import fastapi

from app.api import deps
from app.api.checklist import contract
from app.core.checklist import handle, message
from app.core.user import entity as user_entity

router = fastapi.APIRouter(tags=["Checklist"], prefix="/checklist")


@router.get("")
async def get(user_uid: str = fastapi.Depends(deps.current_user_uid)):  # noqa
    ans = await handle.get_by_user_id(user_uid)
    return contract.GetCheckListResponce(
        user_id=ans.user_id,
        fluorography=ans.fluorography,
        clean=ans.clean,
        no_arrears=ans.no_arrears,
        corpse=ans.corpse,
        documents=ans.documents,
        no_disciplinary=ans.no_disciplinary,
    )


@router.put("")
async def change(  # noqa
    body: contract.ChangeChecklistRequest,
    user_uid: str = fastapi.Depends(deps.current_user_uid),
):
    msg = message.ChangeState(
        user_id=user_entity.UserUID(user_uid), name=body.name.value, state=body.state
    )
    _ = await handle.change_state(msg)
    return
