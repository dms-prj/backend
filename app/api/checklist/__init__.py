import fastapi

from app.api.checklist.endpoints import router


def bootstrap(app: fastapi.FastAPI) -> fastapi.FastAPI:
    """Initialize checklist API."""
    app.include_router(router)
    return app
