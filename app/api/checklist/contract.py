from enum import Enum

import pydantic

from app.core.user import entity as user_entity


class NameEnum(Enum):  # noqa
    fluorography = "fluorography"
    clean = "clean"
    no_arrears = "no_arrears"
    corpse = "corpse"
    documents = "documents"
    no_disciplinary = "no_disciplinary"


class ChangeChecklistRequest(pydantic.BaseModel):  # noqa
    name: NameEnum
    state: bool


class GetCheckListResponce(pydantic.BaseModel):  # noqa
    user_id: user_entity.UserUID

    fluorography: bool
    clean: bool
    no_arrears: bool
    corpse: bool
    documents: bool
    no_disciplinary: bool
