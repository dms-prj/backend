import pydantic


class NewFeedbackRequest(pydantic.BaseModel):  # noqa
    message: str
