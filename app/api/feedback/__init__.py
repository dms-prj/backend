import fastapi

from .endpoints import router


def bootstrap(app: fastapi.FastAPI) -> fastapi.FastAPI:
    """Initialize feedback API."""
    app.include_router(router)
    return app
