import fastapi

from app.api.feedback import contract
from app.core.feedback import entity, handle, message

router = fastapi.APIRouter(prefix="/feedback", tags=["Feedback"])


@router.post("")
async def new_feedback(  # noqa
    body: contract.NewFeedbackRequest,
) -> entity.FeedbackUID:
    msg = message.NewFeedback(message=body.message)
    return await handle.new_feedback(msg)
