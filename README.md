# Backend of Dormitory Management System

To setup project

- Create python3.10 environemnt and **activate it**

```sh
python3.10 -m venv .venv
```

- Install dependencies with poetry

```sh
poetry install
```

- Setup pre-commit

```sh
pre-commit install
```
